# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/03 16:13:53 by cvan-duf          #+#    #+#              #
#    Updated: 2019/05/10 12:09:02 by cvan-duf         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
CC = gcc
CFLAGS = -I./includes -Wall -Wextra -Werror
NAME = fractol
TEST = test
SRCS = fractol srcs/edit_struc srcs/error srcs/mlx_events \
	   srcs/window srcs/init_struct srcs/mandelbrot srcs/gr_draw_ing_img \
	   srcs/julia srcs/move srcs/manage_event srcs/sierpinski \
	   srcs/thread srcs/mandelbrot_3 srcs/mandelbrot_5 srcs/mouse_event

SRCS_FILES = $(addsuffix .c, $(SRCS))

OBJ = $(SRCS_FILES:.c=.o)

MINILIBX = -framework OpenGL -framework AppKit

all: $(NAME)

%.o: %.c ./includes/fractol.h
	$(CC) $(CFLAGS) -c -o $@ $<

$(NAME): $(OBJ)
	make -C minilibx
	make -C libft
	$(CC) $(CFLAGS) -o $(NAME) $(SRCS_FILES) ./libft/libft.a \
		-L ./minilibx/ -lmlx $(MINILIBX)

clean:
	make clean -C libft
	make clean -C minilibx
	rm -f $(OBJ)

fclean: clean
	make fclean -C libft
	rm -f $(NAME)

libft:
	make -C libft

re: fclean all
