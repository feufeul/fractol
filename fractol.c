/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 14:10:49 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 11:56:36 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/fractol.h"

int				main(int argc, char **argv)
{
	t_super		*super;

	if (input_arg(argc, argv) == -1)
		return (error(1));
	if (!(super = new_super(argv[1])))
		return (error(2));
	gr_layout(super);
	mlx_hook(super->win->ptr_win, KP, KPMASK, deal_1, super);
	mlx_hook(super->win->ptr_win, MOTIONNOTIFY, POINTERMOTIONMASK,
		mouse_motion, super);
	mlx_hook(super->win->ptr_win, DESTROYNOTIFY, NOEVENTMASK, quit_prog, super);
	mlx_hook(super->win->ptr_win, BUTTONPRESS, BUTTONPRESSMASK,
		on_mouse, super);
	mlx_hook(super->win->ptr_win, KEYRELEASE, KEYRELEASEMASK,
		when_released, super);
	mlx_loop_hook(super->win->ptr_init, manage_event, super);
	mlx_loop(super->win->ptr_init);
	return (0);
}
