/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 14:26:54 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/14 10:39:36 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include <fcntl.h>
# include <math.h>
# include <pthread.h>
# include "../minilibx/mlx.h"
# include "../libft/libft.h"
# include "keycode.h"

# ifndef INT_MAX
#  define INT_MAX			2147483647
# endif
# ifndef INT_MIN
#  define INT_MIN			-2147483648
# endif

# define NB_THREAD			32
# define WHITE				0xFFFFFF
# define GREY				0x222222
# define RED				0xFF0000
# define GREEN				0x00FF00
# define BLUE				0x0000FF
# define H_WIN				1125
# define L_WIN				2000
# define H_IMG				1125
# define L_IMG				1600
# define X_OFFSET_IMG		400
# define Y_OFFSET_IMG		50
# define COLOR_WIN			WHITE
# define BITS				32
# define ENDIAN				1
# define JULIA				"Julia"
# define J_ITER_MAX			100
# define ZOOM				1
# define MANDELBROT			"Mandelbrot"
# define MANDELBROT3		"Mandelbrot3"
# define MANDELBROT5		"Mandelbrot5"
# define SIERPINSKI			"Sierpinski"
# define M_ITER_MAX			25
# define ZOOM_SPEED			1.1
# define TRANS_SPEED		0.05
# define ZOOM_REL			0.1
# define PRE_SPEED			1

typedef struct				s_state
{
	char					left;
	char					right;
	char					up;
	char					down;
	char					red;
	char					green;
	char					blue;
	char					plus;
	char					minus;
}							t_state;

typedef struct				s_frac
{
	float					c_r;
	float					c_i;
	float					z_r;
	float					z_i;
	float					zoom_x;
	float					zoom_y;
	int						max_x;
	int						max_y;
}							t_frac;

typedef struct				s_p2d
{
	int						x;
	int						y;
}							t_p2d;

typedef struct				s_v2d
{
	int						x1;
	int						y1;
	int						x2;
	int						y2;
}							t_v2d;

typedef struct				s_win
{
	void					*ptr_init;
	void					*ptr_win;
}							t_win;

typedef struct				s_img
{
	int						*address;
	int						bits;
	int						endian;
	int						size;
	void					*ptr;
	double					zoom;
}							t_img;

typedef struct				s_fractal
{
	float					x1;
	float					x2;
	float					y1;
	float					y2;
	float					c_r;
	float					c_i;
	int						iter_max;
	char					locked;
}							t_fractal;

typedef struct				s_iter
{
	int						iter;
	pthread_mutex_t			mutex;
}							t_iter;

typedef struct				s_super
{
	t_win					*win;
	t_img					*img;
	char					*f_word;
	t_fractal				fractal;
	t_iter					iteration;
	float					image_x;
	float					image_y;
	float					zoom_x;
	float					zoom_y;
	int						color_r;
	int						color_g;
	int						color_b;
	int						max_prec;
	t_state					state;
}							t_super;

/*
** edit_struct.c
*/

void						init_info_img(t_img *img);
void						set_value_fractal(t_super *super, char *fractal);
t_v2d						new_point(int x1, int y1, int x2, int y2);

/*
** error.c
*/

int							error(int err);
int							free_all(t_super *super, t_win *win, t_img *img);
int							input_arg(int argc, char **argv);

/*
** gr_draw_in_img.c
*/

int							get_color(t_super *super);
void						draw_in_img_manager(t_super *super, int i, int x,
		int y);
void						gr_draw_in_img(int *img, int x, int y, int color);

/*
** init_struct.c
*/

t_p2d						new_p2d(int x, int y);
t_win						*new_win(void);
t_img						*new_img(t_win *win);
t_super						*new_super(char *fractal);

/*
** julia.c
*/

void						julia(t_super *super);

/*
** manage_event.c
*/

int							manage_event(t_super *super);
int							when_released(int key, t_super *super);

/*
** mandelbrot.c
*/

void						mandelbrot(t_super *super);

/*
** mandelbrot_3.c
*/

void						mandelbrot_3(t_super *super);

/*
** mandelbrot_5.c
*/

void						mandelbrot_5(t_super *super);

/*
** mlx_events.c
*/

int							quit_prog(int key, void *var);
int							gr_expose(t_super *super);
int							deal_1(int keycode, t_super *super);

/*
** mouse_evet.c
*/

int							mouse_motion(int x, int y, t_super *super);
int							on_mouse(int button, int x, int y, t_super *super);

/*
** move.c
*/

void						move_img(t_super *super, int x, int y);
void						zoom_in_img(t_super *super, int i);

/*
** sierpinski.c
*/

void						sierpinski(int n, t_v2d v, t_super *super);

/*
** thread.c
*/

pthread_t					create_thread(void *f(void*), t_super *super);
int							init_threads(t_super *super, void*f(void*));

/*
** window.c
*/

void						gr_layout(t_super *super);
void						*gr_new_colored_window(void *ptr_mlx, int height,
		int length, int color);
void						gr_display_frame(t_super *super, t_p2d p2d,
		t_p2d size_frame, int color);

#endif
