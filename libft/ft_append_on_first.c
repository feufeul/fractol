/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_append_on_first.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 11:41:20 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/25 11:41:22 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_append_on_first(char *str1, char *str2)
{
	char	*final_str;

	final_str = ft_strjoin(str1, str2);
	ft_strdel(&str1);
	return (final_str);
}
