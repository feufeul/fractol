/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 15:52:53 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:54:25 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_is_special_blank(char c)
{
	if (c == '\t' || c == '\n' || c == '\r' || c == '\f' ||
			c == '\v' || c == ' ')
		return (1);
	return (0);
}

int			ft_atoi(const char *str)
{
	unsigned long	result;
	char			sign;

	while (ft_is_special_blank(*str))
		str++;
	result = 0;
	if (*str != '+' && *str != '-' && (!ft_isdigit(*str)))
		return (0);
	sign = (*str == '-' ? -1 : 1);
	if (*str == '-' && !ft_isdigit(*(++str)))
		return (0);
	else if (*str == '+' && !ft_isdigit(*(++str)))
		return (0);
	while (*str && ft_isdigit(*str))
	{
		if (result > 9223372036854775807)
			return (sign == -1 ? 0 : -1);
		result += (*(str++) - '0');
		if (*str && ft_isdigit(*str))
			result *= 10;
	}
	return ((int)(result * sign));
}
