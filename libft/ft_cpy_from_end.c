/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cpy_from_end.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 12:17:43 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/30 15:49:05 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_cpy_from_end(char *dst, char *src)
{
	char		*final_str;
	int			size_src;
	int			size_dst;

	size_src = ft_strlen(src);
	size_dst = ft_strlen(dst);
	if (size_dst > size_src)
	{
		if (!(final_str = ft_strnew(size_dst)))
			return (NULL);
		while (size_src--)
		{
			size_dst--;
			final_str[size_dst] = src[size_src];
		}
		while (size_dst--)
			final_str[size_dst] = dst[size_dst];
		ft_strdel(&src);
		ft_strdel(&dst);
		return (final_str);
	}
	ft_strdel(&dst);
	return (src);
}
