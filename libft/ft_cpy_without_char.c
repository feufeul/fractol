/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cpy_without_char.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 12:32:07 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/23 13:17:43 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char			*ft_cpy_without_char(char *str, char c)
{
	char		*final_str;
	int			i;
	int			j;
	int			occurences;

	i = 0;
	j = 0;
	occurences = ft_get_nb_of_char_in_word(str, c);
	if (!(final_str = ft_strnew(ft_strlen(str) - occurences)))
		return (NULL);
	while (str[i] && occurences)
	{
		if (str[i] != c)
		{
			printf("%s\n", final_str);
			final_str[j++] = str[i];
			occurences--;
		}
		i++;
	}
	ft_strdel(&str);
	return (final_str);
}
