/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dtoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 14:15:37 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/03/04 11:22:37 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_dtoa(double nb, int pre)
{
	char			*final_str;
	char			*str_i;
	char			*str_tmp;
	int				f_part;
	double			tmp;
	int				modified;

	if (pre == -1)
		pre = 6;
	modified = 0;
	tmp = (nb >= 0 ? nb - (int)nb : -(nb - (int)nb));
	if (tmp != 0 && tmp < 0.1)
	{
		modified++;
		tmp += 0.1;
	}
	f_part = (int)(tmp * ft_exponant(10, pre + 1));
	if (f_part + 1 >= (int)ft_exponant(10, pre + 1))
	{
		f_part = (f_part + 1) / 10;
		nb++;
		modified++;
	}
	if (!(str_i = ft_itoa((int)nb)))
		return (NULL);
	str_tmp = ft_strjoin(str_i, ".");
	ft_strdel(&str_i);
	if (!str_tmp)
		return (NULL);
	if (f_part % 10 >= 5)
		f_part += 10;
	f_part /= 10;
	if (!(str_i = ft_itoa(f_part)))
	{
		ft_strdel(&str_tmp);
		return (NULL);
	}
	if (modified)
		str_i[0] = '0';
	final_str = ft_strjoin(str_tmp, str_i);
	ft_strdel(&str_i);
	ft_strdel(&str_tmp);
	return (final_str);
}
