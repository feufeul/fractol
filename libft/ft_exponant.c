/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exponant.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 18:07:30 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/14 14:12:53 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	ft_exponant(int base, unsigned int exp)
{
	unsigned long	result;

	result = 1;
	while (exp > 0)
	{
		result *= base;
		if (result > 4294967295)
			return (0);
		exp--;
	}
	return ((unsigned int)result);
}
