/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fusion_after_i.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 14:47:40 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/25 16:38:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdio.h"

char			*ft_fusion_after_i(char *str1, char *str2, int i)
{
	char		*final_str;
	int			j;

	j = 0;
	if ((final_str = ft_strnew(ft_strlen(str1) + ft_strlen(str2))))
	{
		while (i-- > 0)
		{
			final_str[j] = str1[j];
			j++;
		}
		while (str2[i])
		{
			final_str[j + i] = str2[i];
			i++;
		}
		while (str1[j])
		{
			final_str[j + i] = str1[j];
			j++;
		}
		return (final_str);
	}
	return (NULL);
}
