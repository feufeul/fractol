/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_digit_base.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 13:54:31 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/03/04 15:32:50 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_get_digit_base(long long nb, size_t base)
{
	size_t		digit;

	digit = 1;
	if (nb < 0)
	{
		nb = -nb;
		digit++;
	}
	while (nb /= base)
		digit++;
	return (digit);
}
