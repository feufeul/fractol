/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_nb_of_char_in_word.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 12:42:38 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/23 12:44:41 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_get_nb_of_char_in_word(char *str, char c)
{
	int			nb;
	int			i;

	nb = 0;
	i = 0;
	if (str)
		while (str[i])
			if (str[i++] == c)
				nb++;
	return (nb);
}
