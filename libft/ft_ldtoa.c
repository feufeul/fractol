/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ldtoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 16:25:54 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/03/04 11:23:28 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_ldtoa(long double nb, int pre)
{
	char			*final_str;
	char			*str_i;
	char			*str_tmp;
	long long		f_part;

	if (pre == -1)
		pre = 6;
	if (!(str_i = ft_lltoa_base((long long)nb, 10)))
		return (NULL);
	f_part = (nb - (long long)nb) * ft_exponant(10, pre);
	str_tmp = ft_strjoin(str_i, ".");
	ft_strdel(&str_i);
	if (!str_tmp)
		return (NULL);
	if (!(str_i = ft_lltoa_base(f_part, 10)))
	{
		ft_strdel(&str_tmp);
		return (NULL);
	}
	final_str = ft_strjoin(str_tmp, str_i);
	ft_strdel(&str_i);
	ft_strdel(&str_tmp);
	return (final_str);
}
