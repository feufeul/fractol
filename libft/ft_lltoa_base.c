/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lltoa_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 13:52:18 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/03/04 15:41:54 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char			*ft_lltoa_base(long long ll, size_t base)
{
	char		*str;
	int			digit;

	digit = ft_get_digit_base(ll, base);
	if (!(str = ft_strnew(digit)))
		return (NULL);
	if (ll == LLONG_MIN && base == 10)
		return (ft_strdup("-9223372036854775808"));
	if (ll < 0)
	{
		if (base == 10)
			str[0] = '-';
		ll = -ll;
	}
	if (!ll)
		str[0] = '0';
	while (ll)
	{
		str[--digit] = (ll % base > 9 ? 'A' + ll % base % 10 : ll % base + '0');
		ll /= base;
	}
/*	while (digit >= 0)
	{
		digit--;
		if (!str[digit])
			str[digit] = '0';
	}*/
	return (str);
}
