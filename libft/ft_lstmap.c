/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/12 15:33:00 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:25:43 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_free_lst(t_list *first)
{
	t_list	*tmp;

	while (first)
	{
		tmp = first->next;
		free(first->content);
		free(first);
		first = tmp->next;
	}
}

static t_list	*ft_first_map(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*first;

	first = NULL;
	if (lst)
	{
		if (!f || !(first = (t_list*)malloc(sizeof(t_list*))))
			return (NULL);
		first = f(ft_lstnew(lst->content, lst->content_size));
	}
	return (first);
}

t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*first;
	t_list	*prev;

	first = ft_first_map(lst, f);
	if (!(prev = (t_list*)malloc(sizeof(t_list*))))
	{
		free(first);
		return (NULL);
	}
	prev = first;
	lst = lst->next;
	while (lst)
	{
		if (!(new = (t_list*)malloc(sizeof(t_list*))))
		{
			ft_free_lst(first);
			return (NULL);
		}
		new = f(ft_lstnew(lst->content, lst->content_size));
		prev->next = new;
		prev = new;
		lst = lst->next;
	}
	return (first);
}
