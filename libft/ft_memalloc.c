/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:54:56 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 10:37:38 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*tab;

	if (!(tab = (char*)malloc(sizeof(char) * size)))
		return (NULL);
	while (size > 0)
		tab[--size] = 0;
	return ((void*)tab);
}
