/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/05 12:04:24 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/11 18:05:07 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char	*str;
	char	*src_str;
	size_t	cpt;

	cpt = 0;
	str = (char*)dst;
	src_str = (char*)src;
	while (cpt < n)
	{
		str[cpt] = src_str[cpt];
		cpt++;
	}
	return (dst);
}
