/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_new_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/10 10:52:58 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:53:01 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_new_str(const char *str)
{
	char		*final_str;
	int			size;
	int			i;

	i = 0;
	size = ft_strlen(str);
	if (!(final_str = ft_strnew(size)))
		return (NULL);
	while (i < size)
	{
		final_str[i] = str[i];
		i++;
	}
	return (final_str);
}
