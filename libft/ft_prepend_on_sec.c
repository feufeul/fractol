/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prepend_on_sec.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 11:41:35 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/01/25 11:43:30 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_prepend_on_sec(char *str1, char *str2)
{
	char	*final_str;

	final_str = ft_strjoin(str1, str2);
	ft_strdel(&str2);
	return (final_str);
}
