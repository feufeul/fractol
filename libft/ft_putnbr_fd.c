/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 11:07:31 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:01:48 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_printing_nb(int digit, int nb, int fd)
{
	int		tmp;

	while (digit > 0)
	{
		digit--;
		tmp = nb / ft_exponant(10, digit);
		ft_putchar_fd(tmp + '0', fd);
		nb = nb - tmp * ft_exponant(10, digit);
	}
}

void		ft_putnbr_fd(int nb, int fd)
{
	if (nb == -2147483648)
		ft_putstr_fd("-2147483648", fd);
	else if (nb == 0)
		ft_putchar_fd('0', fd);
	else
	{
		if (nb < 0)
		{
			nb = -nb;
			ft_putchar_fd('-', fd);
		}
		ft_printing_nb(ft_get_digit(nb), nb, fd);
	}
}
