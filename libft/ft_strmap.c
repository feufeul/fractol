/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 16:45:48 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/13 10:05:43 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		size_s;
	char	*final_str;
	int		i;

	if (s == NULL)
		return (NULL);
	size_s = ft_strlen(s);
	i = 0;
	if (!(final_str = (char*)malloc(sizeof(char) * size_s + 1)))
		return (NULL);
	while (s[i])
	{
		final_str[i] = (char)f(s[i]);
		i++;
	}
	final_str[i] = '\0';
	return (final_str);
}
