/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/04 16:41:29 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/13 12:47:35 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncpy(char *dst, const char *src, size_t len)
{
	unsigned char	*ptr_dst;

	ptr_dst = (unsigned char*)dst;
	if (len == 0)
		return (dst);
	while (*src)
	{
		*ptr_dst = *src;
		src++;
		ptr_dst++;
		len--;
		if (len == 0)
			return (dst);
	}
	while (len != 0)
	{
		*ptr_dst = '\0';
		ptr_dst++;
		len--;
	}
	return (dst);
}
