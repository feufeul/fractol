/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 15:56:04 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 10:35:58 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char		*str;

	if (!(str = (char*)ft_memalloc(size + 1)))
		return (NULL);
	while (size + 1 > 0)
		str[size--] = '\0';
	return (str);
}
