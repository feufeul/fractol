/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/09 14:08:35 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/16 11:07:28 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	char	*ptr_hay;
	size_t	size;
	size_t	i;
	size_t	j;

	ptr_hay = (char*)haystack;
	i = 0;
	size = ft_strlen((char*)needle);
	if (len == 0)
		return (0);
	if (*needle == '\0')
		return ((char*)haystack);
	while (*ptr_hay && i <= len - size)
	{
		j = 0;
		while (ptr_hay[j] == needle[j] && j + i < len)
		{
			if (!needle[++j])
				return (ptr_hay);
		}
		ptr_hay++;
		i++;
	}
	return (0);
}
