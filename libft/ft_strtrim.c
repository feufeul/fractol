/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 10:51:45 by cvan-duf          #+#    #+#             */
/*   Updated: 2018/10/18 11:01:26 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_is_blank(char c)
{
	if (c == ' ' || c == '\n' || c == '\t')
		return (1);
	return (0);
}

char			*ft_strtrim(char const *s)
{
	char			*final_str;
	char			*str;
	unsigned int	i;
	unsigned int	j;

	i = 0;
	if (s == NULL || !(final_str = ft_strdup(s)))
		return (NULL);
	while (ft_is_blank(final_str[i]))
		i++;
	if (final_str[i] == 0)
	{
		free(final_str);
		return (ft_strdup(final_str + i));
	}
	j = ft_strlen(s) - 1;
	while (j > i && ft_is_blank(final_str[j]))
		j--;
	final_str[j + 1] = '\0';
	if (!(str = ft_strdup(final_str + i)))
	{
		free(final_str);
		return (NULL);
	}
	return (str);
}
