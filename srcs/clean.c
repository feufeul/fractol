/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 14:47:05 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/03/11 15:14:17 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int			free_all(t_super *super, t_win *win, t_img *img)
{
	if (img)
	{
		free(img->address);
		free(img->ptr);
		free(img);
	}
	if (win)
	{
		ft_memdel(&(win->ptr_init));
		ft_memdel(&(win->ptr_win));
	}
	if (super)
		free(super);
	return (0);
}
