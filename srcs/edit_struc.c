/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edit_struc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 15:08:34 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:28:48 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

static void		set_color(t_super *super)
{
	super->color_r = 0xFF;
	super->color_g = 0x10;
	super->color_b = 0x10;
	super->state.red = 0;
	super->state.blue = 0;
	super->state.green = 0;
}

void			init_info_img(t_img *img)
{
	img->bits = BITS;
	img->size = L_IMG;
	img->endian = ENDIAN;
}

void			set_value_fractal(t_super *super, char *fractal)
{
	super->f_word = fractal;
	set_color(super);
	super->fractal.y1 = -1.2;
	super->fractal.y2 = 1.2;
	super->fractal.locked = 0;
	super->img->zoom = ZOOM;
	super->fractal.x1 = -2.1;
	super->fractal.x2 = 0.6;
	if (ft_strequ(fractal, MANDELBROT) || ft_strequ(fractal, MANDELBROT3) ||
			ft_strequ(fractal, MANDELBROT5))
		super->fractal.iter_max = M_ITER_MAX;
	else if (ft_strequ(fractal, JULIA))
	{
		super->fractal.iter_max = J_ITER_MAX;
		super->fractal.c_r = 0.285;
		super->fractal.c_i = 0.01;
	}
	else if (ft_strequ(fractal, SIERPINSKI))
	{
		super->fractal.iter_max = J_ITER_MAX;
		super->fractal.c_r = 0.285;
		super->fractal.c_i = 0.01;
	}
}

t_v2d			new_point(int x1, int y1, int x2, int y2)
{
	t_v2d		v;

	v.x1 = x1;
	v.x2 = x2;
	v.y1 = y1;
	v.y2 = y2;
	return (v);
}
