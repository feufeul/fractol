/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 15:16:07 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 11:48:59 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int			error(int err)
{
	if (err == 1)
	{
		ft_putstr("usage : ./fractol <fractal_name> ");
		ft_putendl("(Julia, Mandelbrot, Mandelbrot3, Mandelbrot5, Sierpinski)");
		return (-1);
	}
	ft_putendl("ERROR");
	return (err);
}

int			free_all(t_super *super, t_win *win, t_img *img)
{
	if (img)
	{
		free(img->address);
		free(img->ptr);
		free(img);
	}
	if (win)
	{
		ft_memdel(&(win->ptr_init));
		ft_memdel(&(win->ptr_win));
	}
	if (super)
		free(super);
	return (1);
}

int			input_arg(int argc, char **argv)
{
	if (argc != 2 || (!ft_strequ(argv[1], JULIA) &&
			!ft_strequ(argv[1], MANDELBROT) &&
			!ft_strequ(argv[1], MANDELBROT3) &&
			!ft_strequ(argv[1], MANDELBROT5) &&
			!ft_strequ(argv[1], SIERPINSKI)))
		return (-1);
	return (0);
}
