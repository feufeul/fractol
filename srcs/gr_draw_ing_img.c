/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gr_draw_ing_img.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 13:33:26 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:47:34 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int				get_color(t_super *super)
{
	return (((float)super->color_r) * 0x10000 +
			((float)super->color_g) * 0x100 +
			((float)super->color_b) * 0x1);
}

void			gr_draw_in_img(int *img, int x, int y, int color)
{
	long		pixel;

	pixel = x + y * L_IMG;
	if (x >= 0 && x < L_IMG && y >= 0 && y < H_IMG &&
			pixel >= 0 && pixel < L_IMG * H_IMG &&
			pixel < INT_MAX && pixel > INT_MIN)
		img[pixel] = color;
}

void			draw_in_img_manager(t_super *super, int i, int x, int y)
{
	int			color;

	color = (((float)super->color_r / 0xFF) * (i + 50) * 0x10000 +
			((float)super->color_g / 0xFF) * (i + 50) * 0x100 +
			((float)super->color_b / 0xFF) * (i + 50) * 0x1);
	gr_draw_in_img(super->img->address, x, y, color);
}
