/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 12:37:16 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 11:07:41 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

t_p2d			new_p2d(int x, int y)
{
	t_p2d		p2d;

	p2d.x = x;
	p2d.y = y;
	return (p2d);
}

t_win			*new_win(void)
{
	t_win		*win;

	if (!(win = malloc(sizeof(t_win))))
		return (NULL);
	if (!(win->ptr_init = mlx_init()))
	{
		free(win);
		return (NULL);
	}
	if (!(win->ptr_win = gr_new_colored_window(win->ptr_init,
			H_WIN, L_WIN, COLOR_WIN)))
	{
		free(win->ptr_init);
		free(win);
		return (NULL);
	}
	return (win);
}

t_img			*new_img(t_win *win)
{
	int			max;
	t_img		*img;

	max = L_IMG * H_IMG;
	if (!(img = malloc(sizeof(t_img))))
		return (NULL);
	if (!(img->ptr = mlx_new_image(win->ptr_init, L_IMG, H_IMG)))
	{
		free(img);
		return (NULL);
	}
	init_info_img(img);
	img->address = (int*)mlx_get_data_addr(img->ptr, &(img->bits), &(img->size),
			&(img->endian));
	return (img);
}

t_super			*new_super(char *fractal)
{
	t_super		*super;

	if (!(super = malloc(sizeof(t_super))))
		return (NULL);
	if (!(super->win = new_win()))
	{
		free_all(super, NULL, NULL);
		return (NULL);
	}
	if (!(super->img = new_img(super->win)))
	{
		free_all(super, super->win, NULL);
		return (NULL);
	}
	set_value_fractal(super, fractal);
	super->image_x = 0;
	super->iteration.iter = 0;
	pthread_mutex_init(&(super->iteration.mutex), NULL);
	super->max_prec = 6;
	return (super);
}
