/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 09:30:23 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/08 12:05:44 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int				input_arg(int argc, char **argv)
{
	if (argc != 2 || (!ft_strequ(argv[1], JULIA) &&
			!ft_strequ(argv[1], MANDELBROT) &&
			!ft_strequ(argv[1], MANDELBROT3) &&
			!ft_strequ(argv[1], MANDELBROT5)))
		return (-1);
	return (0);
}
