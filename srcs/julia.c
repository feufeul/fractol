/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 16:45:57 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 09:43:26 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

static void		filling_loop(t_super *super, int x, int y, t_frac f)
{
	int			i;
	float		tmp;

	i = 0;
	f.z_r = x / f.zoom_x + super->fractal.x1;
	f.z_i = y / f.zoom_y + super->fractal.y1;
	while (i < super->fractal.iter_max && f.z_r * f.z_r + f.z_i * f.z_i < 4)
	{
		tmp = f.z_r;
		f.z_r = f.z_r * f.z_r - f.z_i * f.z_i + super->fractal.c_r;
		f.z_i = 2 * f.z_i * tmp + super->fractal.c_i;
		i++;
	}
	draw_in_img_manager(super, i, x, y);
}

void			*filling_img(void *var)
{
	t_super		*super;
	int			x;
	int			y;
	int			max;
	t_frac		f;

	super = (t_super*)var;
	f.zoom_x = L_IMG * super->img->zoom /
		((super->fractal.x2 - super->fractal.x1) * 2);
	f.zoom_y = H_IMG * super->img->zoom * 2 /
		((super->fractal.y2 - super->fractal.y1) * 3);
	x = (super->iteration.iter) * (L_IMG / NB_THREAD) - 1;
	max = (super->iteration.iter + 1) * (L_IMG / NB_THREAD);
	super->iteration.iter++;
	pthread_mutex_unlock(&(super->iteration.mutex));
	while (++x < max)
	{
		y = 0;
		while (y < H_IMG)
			filling_loop(super, x, y++, f);
	}
	pthread_exit(NULL);
}

void			julia(t_super *super)
{
	init_threads(super, filling_img);
}
