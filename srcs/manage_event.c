/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_event.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 17:50:32 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 11:37:22 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

static void	manage_event_2(t_super *super)
{
	if (super->state.plus == 1)
	{
		if (super->state.red && super->color_r < 0xFF)
			super->color_r++;
		if (super->state.green && super->color_g < 0xFF)
			super->color_g++;
		if (super->state.blue && super->color_b < 0xFF)
			super->color_b++;
	}
	gr_expose(super);
}

int			manage_event(t_super *super)
{
	if (super->state.up == 1)
		move_img(super, 0, -1);
	if (super->state.down == 1)
		move_img(super, 0, 1);
	if (super->state.left == 1)
		move_img(super, -1, 0);
	if (super->state.right == 1)
		move_img(super, 1, 0);
	if (super->state.minus == 1)
	{
		if (super->state.red && super->color_r > 0)
			super->color_r--;
		if (super->state.green && super->color_g > 0)
			super->color_g--;
		if (super->state.blue && super->color_b > 0)
			super->color_b--;
	}
	manage_event_2(super);
	return (0);
}

int			when_released(int key, t_super *super)
{
	if (key == UP_KEY)
		super->state.up = 0;
	else if (key == DOWN_KEY)
		super->state.down = 0;
	else if (key == LEFT_KEY)
		super->state.left = 0;
	else if (key == RIGHT_KEY)
		super->state.right = 0;
	else if (key == NKMN_KEY)
		super->state.minus = 0;
	else if (key == NKPL_KEY)
		super->state.plus = 0;
	return (0);
}
