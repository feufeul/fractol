/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/12 11:01:06 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/08 14:02:05 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

static void		filling_loop(t_super *super, int x, int y, t_frac f)
{
	int			i;
	float		tmp;

	f.c_r = (float)x / f.zoom_x + super->fractal.x1;
	f.c_i = (float)y / f.zoom_y + super->fractal.y1;
	f.z_r = 0;
	f.z_i = 0;
	i = 0;
	while (i < super->fractal.iter_max && f.z_r * f.z_r + f.z_i * f.z_i < 4)
	{
		tmp = f.z_r;
		f.z_r = f.z_r * f.z_r - f.z_i * f.z_i + f.c_r;
		f.z_i = 2 * f.z_i * tmp + f.c_i;
		i++;
	}
	draw_in_img_manager(super, i, x, y);
}

void			*filling_mandel(void *var)
{
	t_super		*super;
	int			x;
	int			y;
	int			max;
	t_frac		f;

	super = (t_super*)var;
	f.zoom_x = L_IMG * super->img->zoom /
		((super->fractal.x2 - super->fractal.x1) * 2);
	f.zoom_y = H_IMG * super->img->zoom * 2 /
		((super->fractal.y2 - super->fractal.y1) * 3);
	x = (super->iteration.iter) * (L_IMG / NB_THREAD);
	max = (super->iteration.iter + 1) * (L_IMG / NB_THREAD);
	super->iteration.iter++;
	pthread_mutex_unlock(&(super->iteration.mutex));
	while (x < max)
	{
		y = 0;
		while (y < H_IMG)
			filling_loop(super, x, y++, f);
		x++;
	}
	pthread_exit(NULL);
}

void			mandelbrot(t_super *super)
{
	init_threads(super, filling_mandel);
}
