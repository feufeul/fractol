/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_events.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 16:22:32 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 11:38:55 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int				quit_prog(int key, void *var)
{
	(void)key;
	(void)var;
	exit(0);
}

int				gr_expose(t_super *super)
{
	if (super->img->address)
		mlx_destroy_image(super->win->ptr_init, super->img->ptr);
	if (!(super->img->ptr = mlx_new_image(super->win->ptr_init, L_IMG, H_IMG)))
		return ((int)free_all(super, super->win, super->img));
	super->img->address = (int*)mlx_get_data_addr(super->img->ptr,
			&(super->img->bits), &(super->img->size), &(super->img->endian));
	if (ft_strequ(super->f_word, JULIA))
		julia(super);
	else if (ft_strequ(super->f_word, MANDELBROT))
		mandelbrot(super);
	else if (ft_strequ(super->f_word, MANDELBROT3))
		mandelbrot_3(super);
	else if (ft_strequ(super->f_word, MANDELBROT5))
		mandelbrot_5(super);
	else if (ft_strequ(super->f_word, SIERPINSKI))
		sierpinski(1, new_point(0, 0, 729 * super->img->zoom,
					729 * super->img->zoom), super);
	if (!mlx_put_image_to_window(super->win->ptr_init, super->win->ptr_win,
			super->img->ptr, X_OFFSET_IMG, Y_OFFSET_IMG))
		return ((int)free_all(super, super->win, super->img));
	return (1);
}

static int		deal_2(int keycode, t_super *super)
{
	if (keycode == F5_KEY)
	{
		init_info_img(super->img);
		set_value_fractal(super, super->f_word);
	}
	else if (keycode == Q_KEY && super->fractal.iter_max > 2)
		super->fractal.iter_max -= PRE_SPEED;
	else if (keycode == W_KEY)
		super->fractal.iter_max += PRE_SPEED;
	else if (keycode == NK0_KEY)
		set_value_fractal(super, JULIA);
	else if (keycode == NK1_KEY)
		set_value_fractal(super, MANDELBROT);
	else if (keycode == NK2_KEY)
		set_value_fractal(super, MANDELBROT3);
	else if (keycode == NK3_KEY)
		set_value_fractal(super, MANDELBROT5);
	else if (keycode == NK4_KEY)
		set_value_fractal(super, SIERPINSKI);
	else if (keycode == L_KEY)
		super->fractal.locked = (super->fractal.locked) ? 0 : 1;
	else if (keycode == B_KEY)
		super->state.blue = (super->state.blue) ? 0 : 1;
	return (0);
}

int				deal_1(int keycode, t_super *super)
{
	if (keycode == ESC_KEY)
	{
		free_all(super, super->win, super->img);
		exit(-100);
	}
	else if (keycode == UP_KEY)
		super->state.up = 1;
	else if (keycode == DOWN_KEY)
		super->state.down = 1;
	else if (keycode == LEFT_KEY)
		super->state.left = 1;
	else if (keycode == RIGHT_KEY)
		super->state.right = 1;
	else if (keycode == NKPL_KEY)
		super->state.plus = 1;
	else if (keycode == NKMN_KEY)
		super->state.minus = 1;
	else if (keycode == R_KEY)
		super->state.red = (super->state.red) ? 0 : 1;
	else if (keycode == G_KEY)
		super->state.green = (super->state.green) ? 0 : 1;
	else
		deal_2(keycode, super);
	return (0);
}
