/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_event.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 13:44:03 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/08 13:58:20 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int				mouse_motion(int x, int y, t_super *super)
{
	if (!super->fractal.locked && x > 0 &&
			y > 0 && x <= L_WIN &&
			y <= H_WIN && ft_strequ(super->f_word, JULIA))
	{
		super->fractal.c_r = ((2.0 * (float)x) / (float)L_IMG) - 1.0;
		super->fractal.c_i = ((float)y / (float)H_IMG);
	}
	return (0);
}

int				on_mouse(int button, int x, int y, t_super *super)
{
	if (button == SCROLLUP_KEY && x > 0 && y > 0 && x <= L_WIN && y <= H_WIN)
		zoom_in_img(super, 1);
	if (button == SCROLLDOWN_KEY && x > 0 && y > 0 && x <= L_WIN && y <= H_WIN)
		zoom_in_img(super, -1);
	return (0);
}
