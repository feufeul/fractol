/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/01 15:45:24 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:37:04 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void			move_img(t_super *super, int x, int y)
{
	if (super->img->zoom != 0)
	{
		super->fractal.x1 += (x * 0.1) / super->img->zoom;
		super->fractal.x2 += (x * 0.1) / super->img->zoom;
		super->fractal.y1 += (y * 0.1) / super->img->zoom;
		super->fractal.y2 += (y * 0.1) / super->img->zoom;
	}
}

void			zoom_in_img(t_super *super, int i)
{
	if (i == 1)
		super->img->zoom *= ZOOM_SPEED;
	else if (i == -1)
		super->img->zoom /= ZOOM_SPEED;
}
