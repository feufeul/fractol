/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractals.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/10 10:01:48 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:48:45 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

void	ft_fillrect(t_v2d v, t_super *var)
{
	int		x;
	int		y;
	int		i;

	i = (v.y1 * L_IMG) + v.x1;
	while (i < L_IMG * v.y2)
	{
		x = i % L_IMG;
		y = i / L_IMG;
		if (x > v.x1 && x < v.x2 && y > v.y1 && y < v.y2)
			gr_draw_in_img(var->img->address, x, y, get_color(var));
		if (x < v.x2)
			i++;
		else
			i += (L_IMG - v.x2);
	}
}

void	sierpinski(int n, t_v2d v, t_super *var)
{
	ft_fillrect(new_point(((2 * v.x1) + v.x2) / 3.0, ((2 * v.y1) + v.y2) /
				3.0, ((v.x1 + (2 * v.x2)) / 3.0) - 1, ((v.y1 + (2 * v.y2)) /
					3.0) - 1), var);
	if (n < var->max_prec)
	{
		sierpinski(n + 1, new_point(v.x1, v.y1, (((2 * v.x1) + v.x2) / 3.0),
				(((2 * v.y1) + v.y2) / 3.0)), var);
		sierpinski(n + 1, new_point((((2 * v.x1) + v.x2) / 3.0), v.y1,
				((v.x1 + (2 * v.x2)) / 3.0), (((2 * v.y1) + v.y2) / 3.0)), var);
		sierpinski(n + 1, new_point(((v.x1 + (v.x2 * 2)) / 3.0), v.y1,
				v.x2, (((2 * v.y1) + v.y2) / 3.0)), var);
		sierpinski(n + 1, new_point(v.x1, (((2 * v.y1) + v.y2) / 3.0),
				(((2 * v.x1) + v.x2) / 3.0), ((v.y1 + (2 * v.y2)) / 3.0)), var);
		sierpinski(n + 1, new_point(((v.x1 + (2 * v.x2)) / 3.0),
		(((2 * v.y1) + v.y2) / 3.0), v.x2, ((v.y1 + (v.y2 * 2)) / 3.0)), var);
		sierpinski(n + 1, new_point(v.x1, ((v.y1 + (2 * v.y2)) / 3.0),
				(((2 * v.x1) + v.x2) / 3.0), v.y2), var);
		sierpinski(n + 1, new_point((((2 * v.x1) + v.x2) / 3.0),
					((v.y1 + (2 * v.y2)) / 3.0),
					((v.x1 + (2 * v.x2)) / 3.0), v.y2), var);
		sierpinski(n + 1, new_point(((v.x1 + (2 * v.x2)) / 3.0),
				((v.y1 + (2 * v.y2)) / 3.0), v.x2, v.y2), var);
	}
}
