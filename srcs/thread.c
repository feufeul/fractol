/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 14:19:22 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/08 13:52:20 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

int				init_threads(t_super *super, void *f(void*))
{
	pthread_t	threads[NB_THREAD];
	int			i;

	i = 0;
	super->iteration.iter = 0;
	while (i < NB_THREAD)
	{
		pthread_mutex_lock(&(super->iteration.mutex));
		pthread_create(&threads[i], NULL, f, super);
		i++;
	}
	i = 0;
	while (i < NB_THREAD)
	{
		pthread_join(threads[i], NULL);
		i++;
	}
	return (1);
}
