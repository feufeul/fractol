/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cvan-duf <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 11:12:56 by cvan-duf          #+#    #+#             */
/*   Updated: 2019/05/10 10:49:34 by cvan-duf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fractol.h"

static void	gr_layout_bis(t_super *super, int i)
{
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "          Q : REDUCE ITERATION");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "          W : AUGMENT ITERATION");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   NUMKEY 0 : JULIA");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   NUMKEY 1 : MANDELBROT");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   NUMKEY 2 : MANDELBROT POW 3");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   NUMKEY 3 : MANDELBROT POW 5");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   NUMKEY 4 : SIERPINSKI");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "          L : LOCKMODE ON/OFF");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "         F5 : RESET IMAGE");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "    R, G, B : RGBMODE ON/OFF");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, " RGB ON +/- : MODIFY COLOR");
}

void		gr_layout(t_super *super)
{
	int		i;

	i = 10;
	gr_display_frame(super, new_p2d(0, 0), new_p2d(400, H_WIN), GREY);
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, (L_WIN + 400) / 2,
			i, RED, "     FRACTOL");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 100,
			i, WHITE, "LEGEND :");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 40), WHITE, " DOWN_ARROW : MOVE BOTTOM");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "   UP_ARROW : MOVE TOP");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, " LEFT_ARROW : MOVE LEFT");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "RIGHT_ARROW : MOVE RIGHT");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "  SCROLL UP : ZOOM IN");
	mlx_string_put(super->win->ptr_init, super->win->ptr_win, 50,
			(i += 20), WHITE, "SCROLL DOWN : ZOOM OUT");
	gr_layout_bis(super, i);
}

void		*gr_new_colored_window(void *ptr_init, int height,
					int length, int color)
{
	void	*ptr_win;
	int		i;
	int		j;

	if (!(ptr_win = mlx_new_window(ptr_init, length, height, "FRACTOL")))
		return (NULL);
	i = 0;
	while (i < length)
	{
		j = 0;
		while (j < height)
			mlx_pixel_put(ptr_init, ptr_win, i, j++, color);
		i++;
	}
	return (ptr_win);
}

void		gr_display_frame(t_super *super, t_p2d p2d,
		t_p2d size_frame, int color)
{
	int		i;
	int		j;

	i = p2d.x;
	while (i < size_frame.x)
	{
		j = p2d.y;
		while (j < size_frame.y)
			mlx_pixel_put(super->win->ptr_init, super->win->ptr_win,
					i, j++, color);
		i++;
	}
}
